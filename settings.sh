#!/usr/bin/env bash


# export BASE_IMG=fedora-toolbox:34
# export TARGET_IMG_BASE=toolbox-materiaux
# export MATERIAUX_BASE_IMG_VERSION=dev
# export INSTALL_GUI_LIBS=1
# export MATERIAUX_BUILD_TYPE="Debug"
# export BUILD_FENICS=1

export BASE_IMG=fedora:34
export TARGET_IMG_BASE=materiaux
export MATERIAUX_BASE_IMG_VERSION="v0.3"
export MATERIAUX_BUILD_TYPE="Release"
export BUILD_FENICS=1
export INSTALL_GUI_LIBS=0

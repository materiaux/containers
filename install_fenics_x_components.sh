#!/usr/bin/env bash

PETSC_VERSION=3.14.3
SLEPC_VERSION=3.14.1
PETSC4PY_VERSION=3.14.1
SLEPC4PY_VERSION=3.14.0

# PETSC
export S_PETSC_DIR=$PETSC_DIR && unset PETSC_DIR
cd ${BUILD_DIR}
wget -nc http://ftp.mcs.anl.gov/pub/petsc/release-snapshots/petsc-${PETSC_VERSION}.tar.gz \
    -O petsc-${PETSC_VERSION}.tar.gz
mkdir -p petsc-src
tar -xf petsc-${PETSC_VERSION}.tar.gz -C petsc-src --strip-components 1
cd petsc-src
python3 ./configure --COPTFLAGS="-O2 ${COMPILER_ARCH_FLAG}" \
        --CXXOPTFLAGS="-O2 ${COMPILER_ARCH_FLAG}" \
        --FOPTFLAGS="-O2 ${COMPILER_ARCH_FLAG}" \
        --with-debugging=0 \
        --with-openblas-include=/usr/include/openblas \
        --with-openblas-lib=/usr/lib64/libopenblas.so \
        --download-scalapack \
        --download-metis \
        --download-parmetis \
        --download-ptscotch \
        --download-hypre \
        --download-mumps \
        --download-spai \
        --download-ml \
        --prefix=$S_PETSC_DIR
make && make install
export PETSC_DIR=$S_PETSC_DIR


# SLEPC
export S_SLEPC_DIR=$SLEPC_DIR && unset SLEPC_DIR
cd ${BUILD_DIR}
wget -nc http://slepc.upv.es/download/distrib/slepc-${SLEPC_VERSION}.tar.gz \
    -O slepc-${SLEPC_VERSION}.tar.gz
mkdir -p slepc-src
tar -xf slepc-${SLEPC_VERSION}.tar.gz -C slepc-src --strip-components 1
cd slepc-src
python3 ./configure --prefix=$S_SLEPC_DIR
make SLEPC_DIR=${BUILD_DIR}/slepc-src && make SLEPC_DIR=${BUILD_DIR}/slepc-src install
export SLEPC_DIR=$S_SLEPC_DIR

rm -rf ${BUILD_DIR}/petsc-*
rm -rf ${BUILD_DIR}/slepc-*


# python packages
pip3 install --upgrade --no-cache-dir petsc4py==${PETSC4PY_VERSION}
pip3 install --upgrade --no-cache-dir slepc4py==${SLEPC4PY_VERSION}


# FEniCSX from source

export FENICSX_VERSION=$1
export FENICSX_SRC_DIR=$2
export FENICSX_PREFIX=$3
export FENICSX_BUILD_TYPE=$4

echo $FENICSX_VERSION $FENICSX_SRC_DIR $FENICSX_PREFIX $FENICSX_BUILD_TYPE

mkdir -p $FENICSX_SRC_DIR


# KaHIP
cd $FENICSX_SRC_DIR

KAHIP_DIR=$FENICSX_PREFIX/KaHIP/deploy
export KAHIP_VERSION=623decb
git clone https://github.com/schulzchristian/KaHIP.git && \
cd KaHIP/ && \
git checkout ${KAHIP_VERSION} && \
mkdir build && cd build && \
cmake -G Ninja -DCMAKE_INSTALL_PREFIX=${KAHIP_DIR} -DNONATIVEOPTIMIZATIONS=On .. && \
ninja install
# 

# FENICS python libs (my own minimally modified versions to ensure compatibility)
cd $FENICSX_SRC_DIR
for project in fiat ufl ffcx
do
    if [ ! -d ${project} ]
    then
        git clone https://github.com/mrambausek/${project}.git
        cd ${project}
        git checkout -t origin/$FENICSX_VERSION
        cd ..
    fi
    cd ${project}
    python3 -m pip install -U .
    cd ${FENICSX_SRC_DIR}
done


# FENICS dolfin (my own minimally modified version to ensure compatibility)
cd $FENICSX_SRC_DIR
if [ ! -d dolfinx ]
then
    git clone https://github.com/mrambausek/dolfinx.git
fi
cd dolfinx
mkdir -p build
cd build
cmake -G Ninja -DCMAKE_INSTALL_PREFIX=$FENICSX_PREFIX -DCMAKE_CXX_FLAGS="-O2 ${COMPILER_ARCH_FLAG}" ../cpp
ninja ${MAKEFLAGS} install

source $FENICSX_PREFIX/share/dolfinx/dolfinx.conf

cd ../python
python3 -m pip install .
cd ${FENICSX_SRC_DIR}

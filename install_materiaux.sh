#!/usr/bin/env bash

if [ -z "${MATERIAUX_SRC_DIR}" ]
then
    echo "set MATERIAUX_SRC_DIR"
    export MATERIAUX_SRC_DIR=$(pwd)
fi

if [ -z "${MATERIAUX_HOME}" ]
then
    echo "set MATERIAUX_HOME"
    export MATERIAUX_HOME=/usr/local
fi

if [ -z "${MATERIAUX_BUILD_TYPE}" ]
then
    echo "set MATERIAUX_BUILD_TYPE"
    export MATERIAUX_BUILD_TYPE="Release"
fi

echo "ENV: ${MATERIAUX_SRC_DIR}, ${MATERIAUX_HOME}, ${MATERIAUX_BUILD_TYPE}"

# DOCS are built but at their place. They are quite useless since the cannot be viewed unless they are copied to a shared directory

mkdir -p $MATERIAUX_SRC_DIR && cd $MATERIAUX_SRC_DIR

if [ ! -d "materiaux" ]
then
    git clone https://gitlab.com/materiaux/materiaux.git
    if [ ! -z "$MATERIAUX_VERSION" ]
    then
        cd materiaux && git checkout $MATERIAUX_VERSION
    fi
fi
cd $MATERIAUX_SRC_DIR/materiaux
python3 -m pip install -U -e .
#cd docs && make html

if [ ! -z "${FENICS_HOME}" ]
then
    cd $MATERIAUX_SRC_DIR
    if [ ! -d "dolfincoefficients" ]
    then
        git clone https://gitlab.com/materiaux/dolfincoefficients.git
        if [ ! -z "$DOLFINCOEFFICIENTS_VERSION" ]
        then
            cd dolfincoefficients && git checkout $DOLFINCOEFFICIENTS_VERSION
        fi
    fi
    cd $MATERIAUX_SRC_DIR/dolfincoefficients/cpp
    mkdir -p build && cd build
    cmake -G Ninja -DCMAKE_INSTALL_PREFIX=${MATERIAUX_HOME} -DCMAKE_BUILD_TYPE=${MATERIAUX_BUILD_TYPE} ../
    ninja install
    cd $MATERIAUX_SRC_DIR/dolfincoefficients/cpp/docs && ./make_docs.py
    cd $MATERIAUX_SRC_DIR/dolfincoefficients/python
    python3 -m pip install -U -e .
#     cd docs && ./make_docs.py
    
    cd $MATERIAUX_SRC_DIR
    if [ ! -d "materiauXdolfin" ]
    then
        git clone https://gitlab.com/materiaux/materiauXdolfin.git
        if [ ! -z "$MATERIAUXDOLFIN_VERSION" ]
        then
            cd materiauXdolfin && git checkout $MATERIAUXDOLFIN_VERSION
        fi
    fi
    cd $MATERIAUX_SRC_DIR/materiauXdolfin
    python3 -m pip install -U -e .
#     cd docs && ./make_docs.py
fi

if [ ! -z "${FENICSX_HOME}" ]
then
    cd $MATERIAUX_SRC_DIR
    if [ ! -d "dolfinxcoefficients" ]
    then
        git clone https://gitlab.com/materiaux/dolfinxcoefficients.git
        if [ ! -z "$DOLFINXCOEFFICIENTS_VERSION" ]
        then
            cd dolfinxcoefficients && git checkout $DOLFINXCOEFFICIENTS_VERSION
        fi
    fi
    cd $MATERIAUX_SRC_DIR/dolfinxcoefficients/cpp
    mkdir -p build && cd build
    cmake -G Ninja -DCMAKE_INSTALL_PREFIX=${MATERIAUX_HOME} -DCMAKE_BUILD_TYPE=${MATERIAUX_BUILD_TYPE} ../
    ninja install
    cd $MATERIAUX_SRC_DIR/dolfinxcoefficients/cpp/docs && ./make_docs.py
    cd $MATERIAUX_SRC_DIR/dolfinxcoefficients/python
    python3 -m pip install -U -e .
    cd docs && ./make_docs.py
    
    cd $MATERIAUX_SRC_DIR
    if [ ! -d "materiauXdolfinx" ]
    then
        git clone https://gitlab.com/materiaux/materiauXdolfinx.git
        if [ ! -z "$MATERIAUXDOLFINX_VERSION" ]
        then
            cd materiauXdolfinx && git checkout $MATERIAUXDOLFINX_VERSION
        fi
    fi
    cd $MATERIAUX_SRC_DIR/materiauXdolfinx
    python3 -m pip install -U -e .
    cd docs && ./make_docs.py
fi

cd $MATERIAUX_SRC_DIR

# Materiaux Containers

Various build scripts for containers to directly start working with the pacakges provided by `Materiaux`.
In particular, it ensures compatible versions of FEniCS and FEniCS-X.

The main script `buildah.sh` provided relies on [buildah](https://buildah.io/). Please note that this script is
not heavily tested such that it might need some tweaks/updates to be able to build a certain version of the `Materiaux` 
suite.

OCI-compatible containers are available through the container repo of the gitlab repository.
The corresponding docker registry is `registry.gitlab.com/materiaux/containers` such that images can be pulled via, e.g., 
`docker pull registry.gitlab.com/materiaux/containers/$NAME:$TAG`. THESE CONTAINERS ARE CURRENTLY NOT UPDATED ON A 
REGULAR/PER COMMIT BASIS. HOWEVER, THEY ARE USUALLY TESTED BEFORE UPLOADING. Feel free to file some issue if you 
encounter problems.

Note that the containers feature builds of the API docs. However, to be viewable from the host system, 
they have to be moved to a shared directory.

#!/usr/bin/env bash

# FEniCS from source

echo "ENV: $FENICS_VERSION $FENICS_SRC_DIR $FENICS_HOME $FENICS_BUILD_TYPE"

mkdir -p $FENICS_SRC_DIR && cd $FENICS_SRC_DIR

pip3 install -U git+https://github.com/mrambausek/fiat.git@"${FENICS_VERSION}"
pip3 install -U git+https://github.com/mrambausek/ufl.git@"${FENICS_VERSION}"
pip3 install -U git+https://github.com/mrambausek/ffcx.git@"${FENICS_VERSION}"
pip3 install -U git+https://bitbucket.org/fenics-project/dijitso.git@"${FENICS_VERSION}"
pip3 install -U git+https://bitbucket.org/rambausek/ffc.git@"${FENICS_VERSION}"
git clone https://bitbucket.org/rambausek/dolfin.git $FENICS_SRC_DIR/dolfin

# dolfin cpp
cd $FENICS_SRC_DIR/dolfin
mkdir -p build && cd build
cmake -G Ninja -DCMAKE_INSTALL_PREFIX=${FENICS_HOME} -DCMAKE_BUILD_TYPE=${FENICS_BUILD_TYPE} \
        -Wno-dev -DPYTHON_EXECUTABLE:FILEPATH=$(which python3) -DCMAKE_CXX_FLAGS="-O2 ${COMPILER_ARCH_FLAG}" \
        ..
ninja
ninja install


# dolfin python
cd $FENICS_SRC_DIR/dolfin/python
DOLFIN_DIR=${FENICS_HOME}/share/dolfin/cmake python3 -m pip install -v --no-deps --upgrade .


# demos
cd $FENICS_SRC_DIR/dolfin/python/demo
python3 ./generate-demo-files.py
mkdir -p $FENICS_HOME/share/dolfin/demo/python
cp -a documented $FENICS_HOME/share/dolfin/demo/python
cp -a undocumented $FENICS_HOME/share/dolfin/demo/python

rm -rf $FENICS_SRC_DIR/dolfin

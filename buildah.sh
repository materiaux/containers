#!/usr/bin/env bash

# SELINUX:
#  One important option is --security-opt label=disable for sharing the home directory.
#  For non-home directories :z or :Z might be enough to make selinux happy.

# Note that jumping across filesystems/partitions is not possible by default.

# Note: containers for dolfinx provided by the FEniCS project behave quite the same

# Configuration: use the the variable declared below. 
#  Moreover, the script will check for BUILD_BASE, BUILD_FENICS and BUILD_FENICS_X to determine whether 
#  to build the corresponding container variant.


if [ -z "${BUILD_DIR}" -o ! -d "${BUILD_DIR}" ]
then
    export BUILD_DIR=/opt/build
fi

if [ -z "${BASE_IMG}" ]
then
    export BASE_IMG=fedora:34
fi

if [ -z "${INSTALL_GUI_LIBS}" ]
then
    export INSTALL_GUI_LIBS=0
fi

if [ -z "${TARGET_IMG_BASE}" ]
then
    export TARGET_IMG_BASE=materiaux
fi

if [ -z "${TARGET_ARCH}" ]
then
    export TARGET_ARCH=sandybridge
fi

if [ -z "${MATERIAUX_VERSION}" ]
then
    # the "version" can be any git checkout arguments
    export MATERIAUX_VERSION="-t origin/stable-v0.3"
fi

if [ -z "${MATERIAUXDOLFIN_VERSION}" ]
then
    # the "version" can be any git checkout arguments
    export MATERIAUXDOLFIN_VERSION="${MATERIAUX_VERSION}"
fi

if [ -z "${DOLFINCOEFFICIENTS_VERSION}" ]
then
    # the "version" can be any git checkout arguments
    export DOLFINCOEFFICIENTS_VERSION="${MATERIAUX_VERSION}"
fi

if [ -z "${MATERIAUXDOLFINX_VERSION}" ]
then
    # the "version" can be any git checkout arguments
    export MATERIAUXDOLFINX_VERSION="${MATERIAUX_VERSION}"
fi

if [ -z "${DOLFINXCOEFFICIENTS_VERSION}" ]
then
    # the "version" can be any git checkout arguments
    export DOLFINXCOEFFICIENTS_VERSION="${MATERIAUX_VERSION}"
fi

if [ -z "${MATERIAUX_BASE_IMG_VERSION}" ]
then
    export MATERIAUX_BASE_IMG_VERSION="v0.3"
fi

if [ -z "${MATERIAUX_IMG_VERSION}" ]
then
    export MATERIAUX_IMG_VERSION=${MATERIAUX_BASE_IMG_VERSION}
fi

if [ -z "${MATERIAUX_BUILD_TYPE}" ]
then
    export MATERIAUX_BUILD_TYPE="Release"
fi


echo "ENVIRONMENT VARS:"
echo "    BASE_IMG: ${BASE_IMG}"
echo "    TARGET_ARCH: ${TARGET_ARCH}"
echo "    TARGET_IMG_BASE: ${TARGET_IMG_BASE}"
echo "    MATERIAUX_BASE_IMG_VERSION: ${MATERIAUX_BASE_IMG_VERSION}"
echo "    MATERIAUX_VERSION: ${MATERIAUX_VERSION}"
echo "    DOLFINCOEFFICIENTS_VERSION: ${DOLFINCOEFFICIENTS_VERSION}"
echo "    MATERIAUXDOLFIN_VERSION: ${MATERIAUXDOLFIN_VERSION}"
echo "    DOLFINXCOEFFICIENTS_VERSION: ${DOLFINXCOEFFICIENTS_VERSION}"
echo "    MATERIAUXDOLFINX_VERSION: ${MATERIAUXDOLFINX_VERSION}"
echo "    MATERIAUX_BUILD_TYPE: ${MATERIAUX_BUILD_TYPE}"
echo "    INSTALL_GUI_LIBS: ${INSTALL_GUI_LIBS}"
echo "    BUILD_DIR: ${BUILD_DIR}"
echo "    BUILD_FENICS: ${BUILD_FENICS}"
echo "    BUILD_FENICS_X: ${BUILD_FENICS_X}"


# The basic image with common dependencies
#  Determine whether to build
buildah images "${TARGET_IMG_BASE}-base:${MATERIAUX_BASE_IMG_VERSION}"
if [ "$BUILD_BASE" -o "$?" -eq "125" ]
then
    container=$(buildah from ${BASE_IMG})
    echo $container > current_container
    buildah config --label maintainer="Matthias Rambausek <matthias.rambausek@gmail.com>" $container

    buildah run $container -- /bin/bash -c "\
        dnf -y update &&
        dnf -y install \
            bison \
            make \
            cmake \
            ninja-build \
            which \
            diffutils \
            readline \
            flex \
            gcc-c++ \
            gcc-gfortran \
            libasan \
            gdb \
            git \
            libzip \
            lbzip2 \
            hostname \
            graphviz \
            boost-devel \
            nano \
            pkgconfig \
            redhat-rpm-config \
            wget \
            cmake-gui \
            ccache \
            doxygen \
            git-lfs \
            freetype-devel \
            mpich     \
            mpich-devel \
            hdf5-mpich-devel \
            pcre-devel \
            openblas-devel \
            libpng-devel \
            gmp-devel \
            cln-devel \
            mpfr-devel \
            texlive-updmap-* \
            pybind11-devel \
            python3-devel \
            python3-pip \
            python3-pkgconfig \
            python3-scipy \
            python3-sympy \
            python3-matplotlib \
            python3-matplotlib-gtk3 \
            python3-pytest \
            python3-pybind11 \
            python3-lxml \
            python3-urllib3 \
            pandoc \
            nodejs \
            npm \
            texlive-updmap-* \
            texlive-cm-super \
            texlive-type1cm \
            texlive-stix \
            texlive-siunitx \
            mesa-libGLU
    "
    
    if [ "${INSTALL_GUI_LIBS}" ]
    then
        buildah run $container -- /bin/bash -c "dnf install -y adwaita* libXtst libXScrnSaver e2fsprogs-libs"
    fi
    
    buildah run $container -- /bin/bash -c "dnf clean all"


    export CTR_PATH=$(buildah run $container -- /bin/bash -c 'echo $PATH')
    export CTR_PYTHONPATH=$(buildah run $container -- /bin/bash -c 'echo $PYTHONPATH')
    export CTR_LD_LIBRARY_PATH=$(buildah run $container -- /bin/bash -c 'echo $LD_LIBRARY_PATH')
    export CTR_MAN_PATH=$(buildah run $container -- echo /bin/bash -c 'echo $MAN_PATH')
    export CTR_PKGCONFIG_PATH=$(buildah run $container -- echo /bin/bash -c 'echo $PKGCONFIG_PATH')

    export PETSC_DIR=/opt/petsc
    export SLEPC_DIR=/opt/slepc
    
    buildah config \
        --env LC_ALL=C \
        --env GMSH_VERSION=4.7.1 \
        --env PETSC_VERSION=3.15.2 \
        --env SLEPC_VERSION=3.15.1 \
        --env PETSC4PY_VERSION=3.15.0 \
        --env SLEPC4PY_VERSION=3.15.1 \
        \
        --env COMPILER_ARCH_FLAG=\"-march=${TARGET_ARCH}\" \
        \
        --env BUILD_DIR=${BUILD_DIR} \
        \
        --env OPENBLAS_NUM_THREADS=1 \
        --env OMP_NUM_THREADS=1 \
        --env OPENBLAS_VERBOSE=0 \
        \
        --env MPI_BIN=/usr/lib64/mpich/bin \
        --env MPI_SYSCONFIG=/etc/mpich-x86_64 \
        --env MPI_FORTRAN_MOD_DIR=/usr/lib64/gfortran/modules/mpich \
        --env MPI_INCLUDE=usr/include/mpich-x86_64 \
        --env MPI_LIB=/usr/lib64/mpich/lib \
        --env MPI_MAN=/usr/share/man/mpich-x86_64 \
        --env MPI_PYTHON_SITEARCH=/usr/lib64/python2.7/site-packages/mpich \
        --env MPI_PYTHON2_SITEARCH=/usr/lib64/python2.7/site-packages/mpich \
        --env MPI_PYTHON3_SITEARCH=/usr/lib64/python3.9/site-packages/mpich \
        --env MPI_COMPILER=mpich-x86_64 \
        --env MPI_SUFFIX=_mpich \
        --env MPI_HOME=/usr/lib64/mpich \
        \
        --env PATH=/opt/gmsh/bin:/usr/lib64/mpich/bin:${CTR_PATH} \
        --env PYTHONPATH=/opt/gmsh/lib:${CTR_PYTHONPATH} \
        --env LD_LIBRARY_PATH=$SLEPC_DIR/lib:$PETSC_DIR/lib:/usr/local/lib64:/usr/local/lib:/usr/lib64/mpich/lib:${CTR_LD_LIBRARY_PATH} \
        --env MANPATH=/usr/local/share/man:/usr/share/man/mpich-x86_64:${CTR_MANPATH} \
        --env PKG_CONFIG_PATH=/usr/local/lib64/pkgconfig:/usr/lib64/mpich/lib/pkgconfig:${CTR_PKG_CONFIG_PATH} \
        \
        --env PETSC_DIR=$PETSC_DIR \
        --env SLEPC_DIR=$SLEPC_DIR \
    $container

    buildah run $container /bin/bash -c '
        mkdir -p /home/shared
        mkdir -p ${BUILD_DIR}
    '

    buildah config --env EIGEN_VERSION=3.3.9 $container
    
    # EIGEN
    buildah run $container /bin/bash -c '
        cd ${BUILD_DIR}
        wget https://gitlab.com/libeigen/eigen/-/archive/$EIGEN_VERSION/eigen-$EIGEN_VERSION.tar.gz
        tar -xf eigen-${EIGEN_VERSION}.tar.gz
        cd eigen-$EIGEN_VERSION
        mkdir -p build && cd build
        cmake -DCMAKE_INSTALL_PREFIX=/usr/local -DCMAKE_BUILD_TYPE=Release ../
        make install
        mkdir -p /usr/local/share/eigen-$EIGEN_VERSION
        cp -a ${BUILD_DIR}/eigen-$EIGEN_VERSION/debug /usr/local/share/eigen-$EIGEN_VERSION/
    '

    echo 'python
    import sys
    import os
    sys.path.insert(0, "/usr/local/share/eigen-{:s}/debug/gdb".format(os.environ["EIGEN_VERSION"]))
    from printers import register_eigen_printers
    register_eigen_printers (None)
    end
    ' > /tmp/gdbinit
    buildah copy $container /tmp/gdbinit /root/.gdbinit

    buildah run $container /bin/bash -c 'rm -rf ${BUILD_DIR}/eigen-*'


    # PYTHON ESSENTIAL DEPS
    buildah run $container /bin/bash -c '
        python3 -m pip install --upgrade --no-cache-dir numba
        python3 -m pip install --upgrade --no-cache-dir cffi
        python3 -m pip install --upgrade --no-cache-dir cppimport
        python3 -m pip install --upgrade --no-cache-dir mpi4py
        python3 -m pip install --upgrade --no-cache-dir cython
        cd ${BUILD_DIR} && CC="mpicc" HDF5_MPI="ON" HDF5_DIR="/usr/lib64/mpich/" python3 -m pip install --no-binary=h5py --no-cache-dir h5py
        python3 -m pip install --upgrade --no-cache-dir pygmsh
        python3 -m pip install --upgrade --no-cache-dir meshio
        python3 -m pip install --upgrade --no-cache-dir jupyter
        python3 -m pip install --upgrade --no-cache-dir jupyterlab
        python3 -m pip install --upgrade --no-cache-dir ipyparallel
        python3 -m pip install --upgrade --no-cache-dir ipympl
        python3 -m pip install --upgrade --no-cache-dir jupyter_contrib_nbextensions
        python3 -m pip install --upgrade --no-cache-dir jupyter_nbextensions_configurator
        jupyter contrib nbextension install --system
        jupyter nbextensions_configurator enable --system
        jupyter labextension install @jupyter-widgets/jupyterlab-manager
        jupyter lab build
    '
    

    # PETSC
    buildah run $container /bin/bash -c '
        export S_PETSC_DIR=$PETSC_DIR && unset PETSC_DIR
        cd ${BUILD_DIR}
        wget -nc http://ftp.mcs.anl.gov/pub/petsc/release-snapshots/petsc-${PETSC_VERSION}.tar.gz \
            -O petsc-${PETSC_VERSION}.tar.gz
        mkdir -p petsc-src
        tar -xf petsc-${PETSC_VERSION}.tar.gz -C petsc-src --strip-components 1
        cd petsc-src
        python3 ./configure --COPTFLAGS="-O2 ${COMPILER_ARCH_FLAG}" \
                --CXXOPTFLAGS="-O2 ${COMPILER_ARCH_FLAG}" \
                --FOPTFLAGS="-O2 ${COMPILER_ARCH_FLAG}" \
                --with-debugging=0 \
                --with-openblas-include=/usr/include/openblas \
                --with-openblas-lib=/usr/lib64/libopenblas.so \
                --download-scalapack \
                --download-metis \
                --download-parmetis \
                --download-ptscotch \
                --download-hypre \
                --download-mumps \
                --download-spai \
                --download-ml \
                --prefix=$S_PETSC_DIR
        make && make install

        echo "export PETSC_DIR=${S_PETSC_DIR}"
        export PETSC_DIR=$S_PETSC_DIR

        echo "Install petsc4py..."
        python3 -m pip install --upgrade --no-cache-dir petsc4py==${PETSC4PY_VERSION}

        rm -rf ${BUILD_DIR}/petsc-*
    '

    # SLEPC
    buildah run $container /bin/bash -c '
        export S_SLEPC_DIR=$SLEPC_DIR && unset SLEPC_DIR
        cd ${BUILD_DIR}
        wget -nc http://slepc.upv.es/download/distrib/slepc-${SLEPC_VERSION}.tar.gz \
            -O slepc-${SLEPC_VERSION}.tar.gz
        mkdir -p slepc-src
        tar -xf slepc-${SLEPC_VERSION}.tar.gz -C slepc-src --strip-components 1
        cd slepc-src
        python3 ./configure --prefix=$S_SLEPC_DIR
        make SLEPC_DIR=${BUILD_DIR}/slepc-src && make SLEPC_DIR=${BUILD_DIR}/slepc-src install

        echo "export SLEPC_DIR=${S_SLEPC_DIR}"
        export SLEPC_DIR=$S_SLEPC_DIR

        echo "Install slepc4py"
        python3 -m pip install --upgrade --no-cache-dir slepc4py==${SLEPC4PY_VERSION}

        rm -rf ${BUILD_DIR}/slepc-*
    '
    
    # Python doc build tools for materiaux
    buildah run $container /bin/bash -c '
        python3 -m pip install --upgrade --no-cache-dir Sphinx
        python3 -m pip install --upgrade --no-cache-dir sphinx_rtd_theme
        python3 -m pip install --upgrade --no-cache-dir breathe
        python3 -m pip install --upgrade --no-cache-dir nbsphinx
        python3 -m pip install --upgrade --no-cache-dir sphinxcontrib-svg2pdfconverter
        python3 -m pip install --upgrade --no-cache-dir sphinxcontrib-bibtex
        python3 -m pip install --upgrade --no-cache-dir recommonmark
    '

    # GMSH 4
    buildah run $container /bin/bash -c '
        wget http://gmsh.info/bin/Linux/gmsh-$GMSH_VERSION-Linux64-sdk.tgz -O /tmp/gmsh-$GMSH_VERSION-Linux64-sdk.tgz
        cd /tmp && tar -xf gmsh-$GMSH_VERSION-Linux64-sdk.tgz
        mv gmsh-$GMSH_VERSION-Linux64-sdk /opt/gmsh
    '

    buildah run $container /bin/bash -c 'rm /tmp/gmsh-$GMSH_VERSION-Linux64-sdk.tgz'
    buildah run $container /bin/bash -c 'rm -rf ${BUILD_DIR}/*.tgz ${BUILD_DIR}/*.tar.gz'

    buildah config --workingdir /home/shared $container

    buildah commit $container "${TARGET_IMG_BASE}-base:${MATERIAUX_BASE_IMG_VERSION}"
    echo "COMPLETED IMAGE ${TARGET_IMG_BASE}-base:${MATERIAUX_BASE_IMG_VERSION}"
fi


# Specializations

## FENICS master
if [ "${BUILD_FENICS}" ]
then
    buildah images "${TARGET_IMG_BASE}-base-fenics:${MATERIAUX_BASE_IMG_VERSION}"
    if [ "$?" -eq "125" ]
    then
        container=$(buildah from "${TARGET_IMG_BASE}-base:${MATERIAUX_BASE_IMG_VERSION}")
        echo $container > current_container
        echo "${container}"

        export FENICS_VERSION=master
        export FENICS_SRC_DIR=$(buildah run $container -- /bin/bash -c 'echo $BUILD_DIR')
        export FENICS_HOME=/usr/local
        export FENICS_BUILD_TYPE=Release

        buildah config \
            --env FENICS_VERSION=$FENICS_VERSION \
            --env FENICS_HOME=$FENICS_HOME \
            --env FENICS_SRC_DIR=$FENICS_SRC_DIR \
            --env FENICS_BUILD_TYPE=$FENICS_BUILD_TYPE \
            --env DIJITSO_CACHE_DIR=${FENICS_HOME}/.cache/dijitso \
        $container

        buildah copy $container install_fenics_components.sh $FENICS_SRC_DIR/install_fenics_components.sh
        buildah run $container /bin/bash -c '
            bash ${FENICS_SRC_DIR}/install_fenics_components.sh ${FENICS_VERSION} ${FENICS_SRC_DIR} ${FENICS_HOME} ${FENICS_BUILD_TYPE}
        '
        
        buildah commit $container "${TARGET_IMG_BASE}-base-fenics:${MATERIAUX_BASE_IMG_VERSION}"
        echo "COMPLETED IMAGE ${TARGET_IMG_BASE}-base-fenics:${MATERIAUX_BASE_IMG_VERSION}"
    fi
    
    # Materiaux
    echo "USE ${TARGET_IMG_BASE}-base-fenics:${MATERIAUX_BASE_IMG_VERSION}"
    container=$(buildah from ${TARGET_IMG_BASE}-base-fenics:${MATERIAUX_BASE_IMG_VERSION})
    echo $container > current_container
    
    export MATERIAUX_SRC_DIR=$(buildah run $container -- /bin/bash -c 'echo $BUILD_DIR')
    export MATERIAUX_HOME=/usr/local

    buildah config \
        --env MATERIAUX_VERSION="${MATERIAUX_VERSION}" \
        --env MATERIAUXDOLFIN_VERSION="${MATERIAUXDOLFIN_VERSION}" \
        --env DOLFINCOEFFICIENTS_VERSION="${DOLFINCOEFFICIENTS_VERSION}" \
        --env MATERIAUX_HOME="${MATERIAUX_HOME}" \
        --env MATERIAUX_SRC_DIR="${MATERIAUX_SRC_DIR}" \
        --env MATERIAUX_BUILD_TYPE="${MATERIAUX_BUILD_TYPE}" \
    $container

    buildah copy $container install_materiaux.sh $MATERIAUX_SRC_DIR/install_materiaux.sh
    buildah run $container /bin/bash -c 'chmod +x ${MATERIAUX_SRC_DIR}/install_materiaux.sh'
    buildah run $container /bin/bash -c '${MATERIAUX_SRC_DIR}/install_materiaux.sh'
 
    buildah commit $container "${TARGET_IMG_BASE}-fenics:${MATERIAUX_IMG_VERSION}"
    echo "COMPLETED IMAGE ${TARGET_IMG_BASE}-fenics:${MATERIAUX_IMG_VERSION}"
fi


############################
### UNTESTED
############################
## FENICS-X 
# if [ "${BUILD_FENICS_X}" ]
# then
#     container=$(buildah from ${TARGET_IMG_BASE}-base:${MATERIAUX_BASE_IMG_VERSION})
#     echo $container > current_container
# 
#     export FENICSX_VERSION=stable
#     export FENICSX_SRC_DIR=$(buildah run $container -- /bin/bash -c 'echo $BUILD_DIR')
#     export FENICSX_HOME=/usr/local
#     export FENICSX_BUILD_TYPE=Release
# 
#     buildah config \
#         --env FENICSX_VERSION=$FENICSX_VERSION \
#         --env FENICSX_HOME=$FENICSX_HOME \
#         --env FENICSX_SRC_DIR=$FENICSX_SRC_DIR \
#         --env FENICSX_BUILD_TYPE=$FENICSX_BUILD_TYPE \
#         --env FENICSX_CACHE_DIR=$FENICSX_HOME/.cache/fenics \
#         --env KAHIP_ROOT=/opt/KaHIP \
#     $container
# 
#     buildah copy $container install_fenics_x_components.sh $FENICSX_SRC_DIR/install_fenics_x_components.sh
#     buildah run $container /bin/bash -c '
#         bash ${FENICSX_SRC_DIR}/install_fenics_x_components.sh ${FENICSX_VERSION} ${FENICSX_SRC_DIR} ${FENICSX_HOME} ${FENICSX_BUILD_TYPE}
#     '
# 
#     # Materiaux
#     export MATERIAUX_SRC_DIR=$(buildah run $container -- /bin/bash -c 'echo $BUILD_DIR')
#     export MATERIAUX_HOME=/usr/local
# 
#     buildah config \
#         --env MATERIAUX_VERSION=$MATERIAUX_VERSION \
#         --env MATERIAUX_HOME=$MATERIAUX_HOME \
#         --env MATERIAUX_SRC_DIR=$MATERIAUX_SRC_DIR \
#         --env MATERIAUX_BUILD_TYPE=$MATERIAUX_BUILD_TYPE \
#     $container
# 
#     buildah copy $container install_materiaux.sh $MATERIAUX_SRC_DIR/install_materiaux.sh
#     buildah run $container /bin/bash -c '
#         bash ${MATERIAUX_SRC_DIR}/install_materiaux.sh ${MATERIAUX_VERSION} ${MATERIAUX_SRC_DIR} ${MATERIAUX_HOME} ${MATERIAUX_BUILD_TYPE}
#     '
# 
#     buildah commit $container ${TARGET_IMG_BASE}-fenicsx:${MATERIAUX_IMG_VERSION}
# fi
